
const goods= {
  type: 1,
  name: '',
  short_name: '',
  code: '',
  category_id: '',
  label: '微信支付,',
  pic_urls: '',
  simple_info: '',
  detail_info: '',
  stocks: '',
  sort: '',
  is_top: '',
  score: ''
}
const rules= {
  name: [
    { required: true, message: '请输入商品标题', trigger: 'blur' },
    { max: 60, message: '长度在60个字符内', trigger: 'blur' }
  ],
  short_name: [
    { max: 12, message: '短标题限12个字内', trigger: 'blur' }
  ],
  simple_info: [
    { max: 20, message: '短标题限200个字内', trigger: 'blur' }
  ],
  unit: [
    { required: true, message: '请输入商品单位', trigger: 'blur' },
  ],
  score: [
    { required: true, message: '请输入商品积分', trigger: 'blur' },
  ],
  stocks: [
    { required: true, message: '请输入商品库存', trigger: 'blur' },
  ],
  pic_urls: [
    { required: true, message: '请选择商品主图', trigger: 'blur' },
  ] 
}


function upDateFormat(params) {
  return {
    id: params.id,
    type: params.type,
    name: params.name,
    short_name: params.short_name,
    code: params.code,
    category_id: params.category_id,
    label: params.label,
    pic_urls: params.pic_urls,
    simple_info: params.simple_info,
    detail_info: params.detail_info,
    score: params.score,
    stocks: params.stocks,
    sort: params.sort,
    is_top: params.is_top
  }
}


export { goods, rules, upDateFormat }

