import request from '@/utils/request'

//运费模板
/**
 * 获取运费模板列表
 * @param {*} params
 */
export function getExpressList(params) {
  return request({
    url: '/merchantShopExpressTemplate',
    method: 'get',
    params
  })
}
/**
 * 获取区域数据
 * @param {*} params
 */
export function getGoodsAddress() {
  return request({
    url: '/goodAddress',
    method: 'get'
  })
}
/**
 * 获取运费模板
 * @param {*} params
 */
export function getExpress(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantShopExpressTemplate/'+id,
    method: 'get',
    params
  })
}
/**
 * 新增运费模板
 * @param {*} params
 */
export function postExpress(params) {
  return request({
    url: '/merchantShopExpressTemplate',
    method: 'post',
    data: params
  })
}
/**
 * 修改运费模板
 * @param {*} params
 */
export function putExpress(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantShopExpressTemplate/'+id,
    method: 'put',
    data: params
  })
}
/**
 * 修改启用的运费模板
 * @param {*} params
 */
export function putExpressSwitch(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantShopExpressTemplates/'+id,
    method: 'put',
    data: params
  })
}
/**
 * 删除运费模板
 * @param {*} params
 */
export function delExpress(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantShopExpressTemplate/'+id,
    method: 'delete',
    data: params
  })
}


//收货信息
/**
 * 获取收货信息
 * @param {*} params
 */
export function getTakeGoods(params) {
  return request({
    url: '/merchantAfterInfo',
    method: 'get',
    params
  })
}
/**
 * 新增收货信息
 * @param {*} params
 */
export function postTakeGoods(params) {
  return request({
    url: '/merchantAfterInfo',
    method: 'post',
    data: params
  })
}
/**
 * 修改收货信息
 * @param {*} params
 */
export function putTakeGoods(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantAfterInfo/'+id,
    method: 'put',
    data: params
  })
}

//基础设置

/**
 * 获取基础设置
 * @param {*} params
 */
export function getAppSetting(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantAppInfo/'+id,
    method: 'get',
    params
  })
}
/**
 * 获取商城分类
 * @param {*} params
 */
export function getShopTypeList(params) {
  return request({
    url: '/merchantShopCategory',
    method: 'get',
    params
  })
}
/**
 * 修改商城基础设置
 * @param {*} params
 */
export function putAppSetting(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantAppInfo/'+id+'?key='+params.key,
    method: 'put',
    data: params
  })
}[ ]

//团购配置

/**
 * 获取团购配置信息
 * @param {*} params
 */
export function getTuanConfig(params) {
  return request({
    url: '/merchantTuanConfig',
    method: 'get',
    params
  })
}
/**
 * 提交团购配置
 * @param {*} params
 */
export function postTuanConfig(params) {
  return request({
    url: '/merchantTuanConfig',
    method: 'post',
    data: params
  })
}

//电子面单

/**
 * 获取电子面单列表
 * @param {*} params
 */
export function getExpDocList(params) {
  return request({
    url: '/merchantElectronics',
    method: 'get',
    params
  })
}
/**
 * 新增电子面单
 * @param {*} params
 */
export function postExpDoc(params) {
  return request({
    url: '/merchantElectronics',
    method: 'post',
    data: params
  })
}
/**
 * 修改电子面单
 * @param {*} params
 */
export function putExpDoc(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantElectronics/'+id,
    method: 'put',
    data: params
  })
}
/**
 * 删除电子面单
 * @param {*} params
 */
export function delExpDoc(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantElectronics/'+id,
    method: 'delete',
    data: params
  })
}

//页面配置

/**
 * 获取页面配置信息
 * @param {*} params
 */
export function getViewConfig(params) {
  return request({
    url: '/merchantDiy',
    method: 'get',
    params
  })
}
/**
 * 修改页面配置信息
 * @param {*} params
 */
export function putViewConfig(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantDiy/'+id,
    method: 'put',
    data: params
  })
}

//闪送

/**
 * 获取闪送信息
 * @param {*} params
 */
export function getShan(params) {
  return request({
    url: '/merchantUnits',
    method: 'get',
    params
  })
}
/**
 * 修改闪送信息
 * @param {*} params
 */
export function putShan(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantUnits/'+id,
    method: 'put',
    data: params
  })
}

//UU跑腿

/**
 * 获取UU跑腿信息
 * @param {*} params
 */
export function getUU(params) {
  return request({
    url: '/merchantUuAccount',
    method: 'get',
    params
  })
}
/**
 * 修改UU跑腿信息
 * @param {*} params
 */
export function postUU(params) {
  delete params.id;
  return request({
    url: '/merchantUuAccount',
    method: 'post',
    data: params
  })
}

//点我达

/**
 * 获取点我达信息
 * @param {*} params
 */
export function getDianwoda(params) {
  return request({
    url: '/dianwoda',
    method: 'get',
    params
  })
}
/**
 * 修改点我达信息
 * @param {*} params
 */
export function postDianwoda(params) {
  delete params.id;
  return request({
    url: '/dianwoda',
    method: 'post',
    data: params
  })
}


//易联云

/**
 * 获取易联云列表
 * @param {*} params
 */
export function getYLYList(params) {
  return request({
    url: '/merchantPrints',
    method: 'get',
    params
  })
}
/**
 * 新增易联云
 * @param {*} params
 */
export function postYLY(params) {
  return request({
    url: '/merchantPrints',
    method: 'post',
    data: params
  })
}
/**
 * 修改易联云
 * @param {*} params
 */
export function putYLY(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantPrints/'+id,
    method: 'put',
    data: params
  })
}
/**
 * 删除易联云
 * @param {*} params
 */
export function delYLY(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantPrints/'+id,
    method: 'delete',
    data: params
  })
}

//分享海报

/**
 * 获取海报
 * @param {*} params
 */
export function getPosters(params) {
  return request({
    url: '/posters',
    method: 'get',
    params
  })
}
/**
 * 海报上传链接
 */
export function posterUrl() {
  return process.env.VUE_APP_BASE_API+'/posters'
}
