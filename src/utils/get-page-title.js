import defaultSettings from '@/settings'

const title = defaultSettings.title || '卷泡-商家后台管理'

export default function getPageTitle(pageTitle) {
  if (pageTitle) {
    return `${pageTitle} - ${title}`
  }
  return `${title}`
}
